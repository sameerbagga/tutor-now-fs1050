'use strict';

let util = require('util');
let path = require('path');
let fs = require('fs');


let readFile = util.promisify(fs.readFile);
let writeFile = util.promisify(fs.writeFile);

let userDbPath = path.resolve('db/users.json');
let userRequestPath = path.resolve('db/activeRequests.json');
let requestIdPath = path.resolve('db/requestId.json');
let historyPath = path.resolve('db/history.json');
let acceptedRequestPath = path.resolve('db/acceptedRequests.json');


/**
 * Reads the file `users.json` and parses its JSON
 */
function readUsers() {
  return readFile(userDbPath)
    .then((json) => {
      return JSON.parse(json);
    });
}

/**
 * Reads the file `requests.json` and parses its JSON
 */

function readRequests() {
  return readFile(userRequestPath)
    .then((json) => {
      return JSON.parse(json);
    });
}

function readAcceptedRequests() {
  return readFile(acceptedRequestPath)
    .then((json) => {
      return JSON.parse(json);
    });
}

function readHistoryRequests() {
  return readFile(historyPath)
    .then((json) => {
      return JSON.parse(json);
    });
}


/** reads the request Id from `requestId.json` */
function readRequestId() {
  return readFile(requestIdPath)
    .then((json) => {
      return JSON.parse(json);
    });
}

/** reads the completed or canceled requests from `history.json` */
function readHistory() {
  return readFile(historyPath)
    .then((json) => {
      return JSON.parse(json);
    });
}


/** writes the incremented requestId */
function writeRequestId() {
  return readRequestId()
    .then((previousId) => {
      let newId = previousId;
      let incrementedId = previousId[0].reqId + 1;
      newId[0].reqId = incrementedId;
      return writeFile(requestIdPath, JSON.stringify(newId, null, 2));
    });
}

/** Auto increment the request Id */
function getRequestId() {
  return readRequestId()
    .then((requests) => {
      let id = requests[0].reqId + 1;
      writeRequestId();
      return id;
    });
}

/**
 * Writes to the `users.json` file
 */
function writeUsers(users) {
  return writeFile(userDbPath, JSON.stringify(users, null, 2));
}

/**
 * Writes to the `requests.json` file
 */
function writeRequests(requests) {
  return writeFile(userRequestPath, JSON.stringify(requests, null, 2));
}

function writeAcceptedRequests(requests) {
  return writeFile(acceptedRequestPath, JSON.stringify(requests, null, 2));
}

function writeHistoryRequests(requests) {
  return writeFile(historyPath, JSON.stringify(requests, null, 2));
}


/**
 * Determines if a user with a particular username already exists or not
 * @param {string} username
 * @returns {Promise<boolean>} whether a user exists or not
 */
function usernameExists(username, email) {
  return readUsers()
    .then((users) => {
      let exists = false;

      users.forEach((user) => {
        if (user.username === username || user.email === email) {
          exists = true;
        }
      });

      return exists;
    });
}


/**
 * Adds a user to the database
 * @param {object} user
 * @returns {Promise<undefined>}
 */
function addUser(user) {
  return readUsers()
    .then((users) => {
      return writeUsers(users.concat(user));
    });
}


/** adds new requests to activerequests.json page */

function addRequest(request) {
  return readRequests()
    .then((requests) => {
      return writeRequests(requests.concat(request));
    });
}

function addAcceptedRequest(request) {
  return readAcceptedRequests()
    .then((requests) => {
      return writeAcceptedRequests(requests.concat(request));
    });
}

function addHistoryRequest(request) {
  return readHistoryRequests()
    .then((requests) => {
      return writeHistoryRequests(requests.concat(request));
    });
}

/**
 * Get user password hash
 * @param {string} username
 * @returns {Promise<string>}
 */
function getUserPasswordHash(username) {
  return readUsers()
    .then((users) => {
      let match;

      users.forEach((user) => {
        if (user.username === username) {
          match = user;
        }
      });

      if (!match) {
        throw new Error('User does not exist.');
      }

      return match;
    });
}


/** read all pending requests by a student */
function content(username) {
  return readRequests()
    .then((requests) => {
      let studentRequest = [];
      requests.forEach((request) => {
        if (request.studentUsername === username) {
          studentRequest = studentRequest.concat(request);
        }
      });
      return studentRequest;
    });
}

function contentAccepted(username) {
  return readAcceptedRequests()
    .then((requests) => {
      let tutorAcceptedRequest = [];
      requests.forEach((request) => {
        if (request.tutorUsername === username) {
          tutorAcceptedRequest = tutorAcceptedRequest.concat(request);
        }
      });
      return tutorAcceptedRequest;
    });
}

function contentRequests(speciality) {
  return readRequests()
    .then((requests) => {
      let incomingRequest = [];
      requests.forEach((request) => {
        if (request.subject === speciality) {
          incomingRequest = incomingRequest.concat(request);
        }
      });
      return incomingRequest;
    });
}

/** read all completed or cancelled requests by a student */
function historyContent(username) {
  return readHistory()
    .then((requests) => {
      let studentRequest = [];
      requests.forEach((request) => {
        if (request.studentUsername === username) {
          studentRequest = studentRequest.concat(request);
        }
      });
      return studentRequest;
    });
}

function updateUser(username, newUserData) {
  return readUsers()
    .then((allUsers) => {
      let dummyUsers = [];
      let updatedUser;

      allUsers.forEach((user) => {
        if (user.username == username) {
          if (user.title === 'student') {
            updatedUser = {
              title: user.title,
              username: user.username,
              password: user.password,
              name: newUserData.fullname,
              email: user.email,
              dateOfBirth: user.dateOfBirth,
              education: newUserData.educationStu,
              state: newUserData.inputState,
              country: newUserData.inputCountry,
            };
          } else if (user.title === 'tutor') {
            updatedUser = {
              title: user.title,
              username: user.username,
              password: user.password,
              name: newUserData.fullname,
              email: user.email,
              speciality: newUserData.speciality,
              education: newUserData.educationTut,
              state: newUserData.inputState,
              country: newUserData.inputCountry,
            };
          }
        } else {
          dummyUsers = dummyUsers.concat(user);
        }
      });
      dummyUsers = dummyUsers.concat(updatedUser);
      // console.log('updated=', updatedUser);
      // console.log('dummyUser=', dummyUsers);
      writeUsers(dummyUsers);
    });
}

module.exports = {
  usernameExists: usernameExists,
  addUser: addUser,
  getUserPasswordHash: getUserPasswordHash,
  addRequest: addRequest,
  getRequestId: getRequestId,
  content: content,
  historyContent: historyContent,
  updateUser: updateUser,
  contentRequests: contentRequests,
  readActiveRequests: readRequests,
  addAcceptedRequest: addAcceptedRequest,
  addHistoryRequest: addHistoryRequest,
  writeRequests: writeRequests,
  contentAccepted: contentAccepted,
  readUsers: readUsers,
};
