'use strict';

let db = require('../../db');

/**
 * Request Tutor
 */
function getRequestTutor(req, res) {
  if (!req.session.username) {
    res
      .status(403)
      .render('status/forbidden');
  } else {
    res.render('request-tutor', {
      pageId: 'request-tutor',
      title: 'Request a Tutor',
      username: req.session.username,
      formValues: {
        topicForHelp: null,
        description: null,
      },
      formErrors: {
        topicForHelp: null,
        description: null,
      },
    });
  }
}

function postRequestTutor(req, res) {
  let formErrors = {
    topicForHelp: (req.body.topicForHelp !== undefined) ? null : 'Select one of the Subject you require assistance with.',
    description: (req.body.description) ? null : 'Add Description of your Query.',
  };

  if (formErrors.topicForHelp || formErrors.description) {
    res
      .status(400)
      .render('request-tutor', {
        pageId: 'request-tutor',
        title: 'Request a Tutor',
        username: req.session.username,
        topicForHelp: req.session.topicForHelp,
        description: req.session.description,
        formErrors: formErrors,
        formValues: {
          topicForHelp: req.body.topicForHelp,
          description: req.body.description,
        },
      });
  } else {
    db.getRequestId()
      .then((id) => {
        let newRequest = {
          reqId: id,
          studentUsername: req.session.username,
          studentEmail: req.session.email,
          subject: req.body.topicForHelp,
          question: req.body.description,
          time: Date(),
        };

        db.addRequest(newRequest)
          .then(() => {
            res
              .status(200)
              .redirect('/studentdash');
          });
      });
  }
}

module.exports = {
  get: getRequestTutor,
  post: postRequestTutor,
};
