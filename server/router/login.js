'use strict';

const passwordHash = require('password-hash');
let db = require('../../db');

/**
 * Initial page rendering
 */
function getLoginRoute(req, res) {
  if (req.session.username) {
    return db.getUserPasswordHash(req.session.username)
      .then(async (dbUser) => {
        if (dbUser.title === 'student') {
          res
            .status(200)
            .redirect('/studentdash');
        } else if (dbUser.title === 'tutor') {
          res
            .status(200)
            .redirect('/tutordash');
        }
      });
  } else {
    res.render('login', {
      pageId: 'login',
      title: 'Login',
      username: req.session.username,
      formError: null,
      formValues: { username: null, password: null },
    });
  }
}

/**
 * Form submission
 */
function postLoginRoute(req, res, next) {
  db.usernameExists(req.body.username)

    // Validate
    .then((usernameExists) => {
      // Login is not valid if username does not exist
      if (!usernameExists) {
        return {
          status: false,
        };

        // If the username exists verify the password is correct
      } else {
        return db.getUserPasswordHash(req.body.username)
          .then(async (dbUser) => {
            if (await passwordHash.verify(req.body.password, dbUser.password)) {
              if (dbUser.title === 'tutor') {
                return {
                  status: true,
                  title: dbUser.title,
                  speciality: dbUser.speciality,
                  email: dbUser.email,
                };
              } else if (dbUser.title === 'student') {
                return {
                  status: true,
                  title: dbUser.title,
                  email: dbUser.email,
                };
              }
            } else {
              console.log('passwrod hash dont match');
              return {
                status: false,
                title: dbUser.title,
              };
            }
          });
      }
    })

    // Render on failure or log user in
    .then((isValid) => {
      // If invalid respond with authentication failure
      if (!isValid.status) {
        res
          .status(401)
          .render('login', {
            pageId: 'login',
            title: 'Login',
            username: req.session.username,
            formError: 'Authentication Failed !!!',
            formValues: {
              username: req.body.username || null,
              password: req.body.password || null,
            },
          });

        // Else log the user in and redirect to home page
      } else {
        req.session.username = req.body.username;
        req.session.title = isValid.title;
        req.session.email = isValid.email;

        if (isValid.title === 'student') {
          res.redirect('/studentdash');
        } else if (isValid.title === 'tutor') {
          req.session.speciality = isValid.speciality;
          res.redirect('/tutordash');
        }

        // res.redirect('/');
      }
    })
    .catch(next);
}


module.exports = {
  get: getLoginRoute,
  post: postLoginRoute,
};
