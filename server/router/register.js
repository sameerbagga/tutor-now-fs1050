'use strict';

const passwordHash = require('password-hash');

let db = require('../../db');

/**
 * Initial page rendering
 */
function getRegisterRoute(req, res) {
  if (req.session.username) {
    return db.getUserPasswordHash(req.session.username)
      .then(async (dbUser) => {
        if (dbUser.title === 'student') {
          res
            .status(200)
            .redirect('/studentdash');
        } else if (dbUser.title === 'tutor') {
          res
            .status(200)
            .redirect('/tutordash');
        }
      });
  } else {
    res.render('register', {
      pageId: 'register',
      title: 'Register',
      username: req.session.username,
      formValues: {
        whoareyou: null,
        fullname: null,
        username: null,
        inputEmail4: null,
        password: null,
        dob: null,
        inputCountry: null,
        inputStateCa: null,
        inputStateUs: null,
        speciality: null,
        educationStu: null,
        educationTut: null,
      },
      formErrors: {
        whoareyou: null,
        fullname: null,
        username: null,
        inputEmail4: null,
        password: null,
        dob: null,
        inputCountry: null,
        inputStateCa: null,
        inputStateUs: null,
        speciality: null,
        educationStu: null,
        educationTut: null,
      },
    });
  }
}


/**
 * Form submission
 */
function postRegisterRoute(req, res, next) {
  // First we check if the username provided already exists
  db.usernameExists(req.body.username, req.body.inputEmail4)
    .then(async (usernameExists) => {
      // Check if form values are valid
      let formErrors = {};

      if (req.body.whoareyou === 'student') {
        formErrors = {
          whoareyou: (req.body.whoareyou === 'student' || req.body.whoareyou === 'tutor') ? null : 'Select one option',
          fullname: (req.body.fullname) ? null : 'Name is required',
          inputEmail4: (!usernameExists && req.body.inputEmail4) ? null : 'Enter a valid Email or Email Already exists',
          username: (!usernameExists && req.body.username) ? null : 'Invalid username or User Already exists',
          password: (req.body.password && req.body.password.length >= 6) ? null : 'Invalid password (must contain 6 or more characters)',
          verifypassword: (req.body.verifypassword && req.body.verifypassword === req.body.password) ? null : 'Both Password should match',
          dob: (req.body.dob) ? null : 'Enter a valid Date of Birth',
          inputCountry: (req.body.inputCountry !== 'selectcountry') ? null : 'Select Valid Country',
          inputStateCa: (req.body.inputStateCa !== 'selectstate') ? null : 'Select valid State',
          inputStateUs: (req.body.inputStateUs !== 'selectstate') ? null : 'Select valid State',
          educationStu: (req.body.educationStu !== undefined) ? null : 'Please select Education Level',
        };
      } else if (req.body.whoareyou === 'tutor') {
        formErrors = {
          whoareyou: (req.body.whoareyou !== undefined) ? null : 'Select one option',
          fullname: (req.body.fullname) ? null : 'Name is required',
          inputEmail4: (!usernameExists && req.body.inputEmail4) ? null : 'Enter a valid Email or Email Already exists',
          username: (!usernameExists && req.body.username) ? null : 'Invalid username or User Already exists',
          password: (req.body.password && req.body.password.length >= 6) ? null : 'Invalid password (must contain 6 or more characters)',
          verifypassword: (req.body.verifypassword && req.body.verifypassword === req.body.password) ? null : 'Both Password should match',
          inputCountry: (req.body.inputCountry !== 'selectcountry') ? null : 'Select Valid Country',
          inputStateCa: (req.body.inputStateCa !== 'selectstate') ? null : 'Select valid State',
          inputStateUs: (req.body.inputStateUs !== 'selectstate') ? null : 'Select valid State',
          speciality: (req.body.speciality !== undefined) ? null : 'Please make one speciality',
          educationTut: (req.body.educationTut !== undefined) ? null : 'Please select Education Level',
        };
      }


      // If there are any errors do not register the user
      // eslint-disable-next-line max-len
      if (formErrors.whoareyou || formErrors.username || formErrors.password || formErrors.fullname || formErrors.inputEmail4 || formErrors.verifypassword || formErrors.dob || formErrors.inputCountry || formErrors.inputState || formErrors.speciality || formErrors.education) {
        res
          .status(400)
          .render('register', {
            pageId: 'register',
            title: 'Register',
            username: req.session.username,
            whoareyou: req.session.whoareyou,
            formErrors: formErrors,
            formValues: {
              whoareyou: req.session.whoareyou,
              username: req.session.username,
              password: req.body.password,
              fullname: req.body.fullname,
              inputEmail4: req.body.inputEmail4,
              dob: req.body.dob,
              schoolprog: req.body.schoolprog,
              inputCountry: req.body.inputCountry,
              inputStateCa: req.body.inputStateCa,
              inputStateUs: req.body.inputStateUs,
              speciality: req.body.speciality,
              educationStu: req.body.educationStu,
              educationTut: req.body.educationTut,
            },
          });

        // console.log(req.body.educationStu);
        // Else, the form values are valid
      } else {
        // If successful should redirect to `/login`
        const hash = await passwordHash.generate(req.body.password);
        let newUser = {};
        let state = (req.body.inputCountry == 'CA') ? req.body.inputStateCa : req.body.inputStateUs;

        if (req.body.whoareyou === 'student') {
          newUser = {
            title: 'student',
            username: req.body.username,
            password: hash,
            name: req.body.fullname,
            email: req.body.inputEmail4,
            dateOfBirth: req.body.dob,
            education: req.body.educationStu,
            state: state,
            country: req.body.inputCountry,
          };
        } else if (req.body.whoareyou === 'tutor') {
          newUser = {
            title: 'tutor',
            username: req.body.username,
            password: hash,
            name: req.body.fullname,
            email: req.body.inputEmail4,
            speciality: req.body.speciality,
            education: req.body.educationTut,
            state: state,
            country: req.body.inputCountry,
          };
        }

        db.addUser(newUser)
          .then(() => {
            res
              .status(200)
              .redirect('/login');
          });
      }
    })
    .catch(next);
}


module.exports = {
  get: getRegisterRoute,
  post: postRegisterRoute,
};
