'use strict';

let db = require('../../db');

/**
 * New-Requests page
 */
function newRequestRoute(req, res) {
  if (!req.session.username) {
    res
      .status(403)
      .render('status/forbidden');
  } else {
    return db.contentRequests(req.session.speciality)
      .then((requests) => {
        res.render('new-requests', {
          pageId: 'new-requests',
          title: 'Incoming Requests',
          username: req.session.username,
          requests: requests,
        });
      });
  }
}

function acceptRequest(req, res) {
  // console.log('id=', req.body.reqId);
  return db.readActiveRequests()
    .then((requests) => {
      // console.log('before=', requests);
      // console.log('length=', requests.length);
      let acceptedRequest;
      let dummyRequests = [];
      requests.forEach((request) => {
        // console.log('test-loop=', request.reqId);
        if (request.reqId == req.body.reqId) {
          // console.log('test=', request);
          acceptedRequest = request;
        } else {
          dummyRequests = dummyRequests.concat(request);
        }
      });
      acceptedRequest.tutorUsername = req.session.username;
      acceptedRequest.tutorEmail = req.session.email;
      acceptedRequest.acceptedTime = Date();
      db.addAcceptedRequest(acceptedRequest);
      db.addHistoryRequest(acceptedRequest);
      db.writeRequests(dummyRequests);
      // console.log('deleted=', acceptedRequest);
      // console.log('remaining=', dummyRequests);
      res.redirect('/new-requests');
    });
}

module.exports = {
  get: newRequestRoute,
  post: acceptRequest,
};
