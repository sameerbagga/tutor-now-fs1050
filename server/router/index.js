'use strict';

let express = require('express');
let homeRoutes = require('./home');
let loginRoutes = require('./login');
let logoutRoutes = require('./logout');
let registerRoutes = require('./register');
let studentdashRoutes = require('./studentdash');
let requestTutorRoutes = require('./request-tutor');
let pendingRequestRoutes = require('./pendingrequest');
let historyRoutes = require('./history');
let profileRoutes = require('./profile');
let tutordashRoutes = require('./tutordash');
let newRequestRoutes = require('./new-requests');
let acceptedRequestRoutes = require('./accepted-requests');
let tutorSearchRoutes = require('./tutorSearch');

// Create instance of an express router
let router = express.Router();

/**
 * Define routes
 */

// Home page
router.get('/', homeRoutes.get);


// Login page
router.get('/login', loginRoutes.get);
router.post('/login', loginRoutes.post);

// Logout
router.get('/logout', logoutRoutes.get);

// Register page
router.get('/register', registerRoutes.get);
router.post('/register', registerRoutes.post);

//Student Dashboard Page
router.get('/studentdash', studentdashRoutes.get);

//Request Tutor Page
router.get('/request-tutor', requestTutorRoutes.get);
router.post('/request-tutor', requestTutorRoutes.post);

//Pending Request Page
router.get('/pendingrequest', pendingRequestRoutes.get);
router.post('/pendingrequest', pendingRequestRoutes.post);

//History Page
router.get('/history', historyRoutes.get);


//Profile Page
router.get('/profile', profileRoutes.get);
router.post('/profile', profileRoutes.post);

// Tutor Dashboard
router.get('/tutordash', tutordashRoutes.get);

//Incoming Requests
router.get('/new-requests', newRequestRoutes.get);
router.post('/new-requests', newRequestRoutes.post);

//Accepted Request Page
router.get('/accepted-requests', acceptedRequestRoutes.get);

//Tutor Search Page
router.get('/tutorSearch', tutorSearchRoutes.get);
router.post('/tutorSearch', tutorSearchRoutes.post);

module.exports = router;
