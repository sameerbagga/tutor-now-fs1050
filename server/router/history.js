'use strict';

let db = require('../../db');

/**
 * Pending Requests page
 */
function historyRoute(req, res) {
  if (!req.session.username) {
    res
      .status(403)
      .render('status/forbidden');
  } else {
    return db.historyContent(req.session.username)
      .then((requests) => {
        res.render('history', {
          pageId: 'history',
          title: 'Past Requests',
          username: req.session.username,
          requests: requests,
        });
      });
  }
}

module.exports = { get: historyRoute };
