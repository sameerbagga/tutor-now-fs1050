'use strict';

let db = require('../../db');
/**
 * Home page
 */
function getHomeRoute(req, res) {
  if (req.session.username) {
    return db.getUserPasswordHash(req.session.username)
      .then(async (dbUser) => {
        if (dbUser.title === 'student') {
          res
            .status(200)
            .redirect('/studentdash');
        } else if (dbUser.title === 'tutor') {
          res
            .status(200)
            .redirect('/tutordash');
        }
      });
  } else {
    res.render('home', {
      pageId: 'home',
      title: 'Home',
      username: req.session.username,
    });
  }
}


module.exports = { get: getHomeRoute };
