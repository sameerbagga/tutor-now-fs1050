'use strict';

let db = require('../../db');

/**
 * Accepted Requests page
 */
function acceptedRequestRoute(req, res) {
  if (!req.session.username) {
    res
      .status(403)
      .render('status/forbidden');
  } else {
    return db.contentAccepted(req.session.username)
      .then((requests) => {
        res.render('accepted-requests', {
          pageId: 'accepted-requests',
          title: 'Accepted Requests',
          username: req.session.username,
          requests: requests,
        });
      });
  }
}

module.exports = { get: acceptedRequestRoute };
