'use strict';

// const passwordHash = require('password-hash');

let db = require('../../db');

/**
 * Initial page rendering
 */
function getProfileRoute(req, res) {
  if (!req.session.username) {
    res
      .status(403)
      .render('status/forbidden');
  } else {
    return db.getUserPasswordHash(req.session.username)
      .then(async (dbUser) => {
        res.render('profile', {
          pageId: 'profile',
          title: 'User Profile',
          username: req.session.username,
          titlexx: req.session.title,
          formValues: {
            fullname: dbUser.name,
            username: dbUser.username,
            inputEmail4: dbUser.email,
            dob: dbUser.dateOfBirth,
            inputCountry: dbUser.country,
            inputState: dbUser.state,
            speciality: dbUser.speciality,
            educationStu: dbUser.education,
            educationTut: dbUser.education,
          },
        });
      });
  }
}

function updateUserRoute(req, res) {
  db.updateUser(req.session.username, req.body)
    .then(() => {
      res
        .status(200)
        .redirect('/');
    });
}


module.exports = {
  get: getProfileRoute,
  post: updateUserRoute,
};
