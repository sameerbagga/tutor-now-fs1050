'use strict';

/**
 * Student Dashboard
 */
function getTutorDash(req, res) {
  if (!req.session.username) {
    res
      .status(403)
      .render('status/forbidden');
  } else {
    res.render('tutordash', {
      pageId: 'tutordash',
      title: 'Tutor Dashboard',
      username: req.session.username,
    });
  }
}


module.exports = { get: getTutorDash };
