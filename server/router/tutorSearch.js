'use strict';

let db = require('../../db');

/**
 * Tutor Search
 */
function getTutorList(req, res) {
  if (!req.session.username) {
    res
      .status(403)
      .render('status/forbidden');
  } else {
    res.render('tutorSearch', {
      pageId: 'tutorSearch',
      title: 'Tutor Search',
      username: req.session.username,
      tutors: [],
      formvalues: { topicForHelp: null },
    });
  }
}

function tutorSearch(req, res) {
  db.readUsers()
    .then((users) => {
      let tutors = [];
      users.forEach((user) => {
        if (user.title === 'tutor' && user.speciality == req.body.topicForHelp) {
          tutors = tutors.concat(user);
        }
      });
      res.render('tutorSearch', {
        pageId: 'tutorSearch',
        title: 'Tutor Search',
        username: req.session.username,
        tutors: tutors,
        formvalues: { topicForHelp: req.body.topicForHelp },
      });
    });
}

module.exports = {
  get: getTutorList,
  post: tutorSearch,
};
