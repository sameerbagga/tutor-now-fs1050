'use strict';

let db = require('../../db');

/**
 * Pending Requests page
 */
function pendingRequestRoute(req, res) {
  if (!req.session.username) {
    res
      .status(403)
      .render('status/forbidden');
  } else {
    return db.content(req.session.username)
      .then((requests) => {
        res.render('pendingrequest', {
          pageId: 'pendingrequest',
          title: 'Pending Requests',
          username: req.session.username,
          requests: requests,
        });
      });
  }
}

function cancelRequest(req, res) {
  return db.readActiveRequests()
    .then((requests) => {
      let cancelledRequest;
      let dummyRequests = [];
      requests.forEach((request) => {
        if (request.reqId == req.body.reqId) {
          cancelledRequest = request;
        } else {
          dummyRequests = dummyRequests.concat(request);
        }
      });
      cancelledRequest.tutorUsername = '';
      cancelledRequest.tutorEmail = '';
      cancelledRequest.acceptedTime = Date();
      db.addHistoryRequest(cancelledRequest);
      db.writeRequests(dummyRequests);
      res.redirect('/pendingrequest');
    });
}

module.exports = {
  get: pendingRequestRoute,
  post: cancelRequest,
};
