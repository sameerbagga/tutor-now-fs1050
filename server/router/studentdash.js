'use strict';

/**
 * Student Dashboard
 */
function getStudentDash(req, res) {
  if (!req.session.username) {
    res
      .status(403)
      .render('status/forbidden');
  } else {
    res.render('studentdash', {
      pageId: 'studentdash',
      title: 'Student Dashboard',
      username: req.session.username,
    });
  }
}


module.exports = { get: getStudentDash };
